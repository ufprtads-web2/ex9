/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Atendimento;
import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.beans.Produto;
import com.ufpr.tads.web2.beans.Usuario;
import com.ufpr.tads.web2.beans.TipoAtendimento;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cassiano
 */
public class AtendimentosFacadeTest {
    
    public int atendimento_id = 1; 

    public int getAtendimento_id() {
        return atendimento_id;
    }

    public void setAtendimento_id(int atendimento_id) {
        this.atendimento_id = atendimento_id;
    }
    
    public AtendimentosFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }   
    
    /**
     * Test of addAtendimento method, of class AtendimentosFacade.
     * @throws java.lang.Exception
     */
    @Test
    public void testAddAtendimento() throws Exception {
        System.out.println("addAtendimento with id = 6");
        Atendimento atendimento = new Atendimento();
        Usuario user = new Usuario();
        Cliente cli = new Cliente();
        TipoAtendimento tipo = new TipoAtendimento();
        Produto prod = new Produto();
        
        user.setId_usuario(2);
        cli.setId(2);
        tipo.setId(1);
        prod.setId(2);
        
        atendimento.setDsc("desc");        
        atendimento.setCliente(cli);
        atendimento.setUsuario(user);
        atendimento.setProduto(prod);
        atendimento.setTipo_atendimento(tipo);
        atendimento.setResposta("s");   
        atendimento.setId(6);
        
        try {
            AtendimentosFacade.addAtendimento(atendimento);             
            assertEquals(this, this);   
        } catch (SQLException e) {            
            fail(e.getMessage());
        }
             
    }

//    /**
//     * Test of updateAtendimento method, of class AtendimentosFacade.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testUpdateAtendimento() throws Exception {
//        System.out.println("updateAtendimento to resposta = n");
//        Atendimento atendimento = new Atendimento();
//        atendimento.setId(1);
//        atendimento.setDsc("UNIT TEST");
//        atendimento.setDt_hr(null);
//        atendimento.setId_cliete(2);
//        atendimento.setId_usuario(2);
//        atendimento.setId_produto(1);
//        atendimento.setId_tipo_atendimento(1);
//        atendimento.setResposta("n");   
//        try {
//            AtendimentosFacade.updateAtendimento(atendimento);
//        } catch (SQLException e) {
//           fail(e.getMessage());           
//        }
//        assertEquals(this, this);
//    }
//    
//    /**
//     * Test of getAtendimento method, of class AtendimentosFacade.
//     * @throws java.lang.Exception
//     */    
//    @Test
//    public void testGetAtendimento() throws Exception {
//        System.out.println("getAtendimento where id = 2");
//        String id = "2";
//        int expRes = 2;
//        Atendimento result = AtendimentosFacade.getAtendimento(id);
//        assertEquals(expRes, result.getId());
//    }
//
    /**
     * Test of deleteAtendimento method, of class AtendimentosFacade.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteAtendimento() throws Exception {
        System.out.println("deleteAtendimento where id = 6");
        String id = "6";
        try {
            AtendimentosFacade.deleteAtendimento(id);
        } catch (SQLException e) {
            fail(e.getMessage());
        }
        assertEquals(this, this);
    }


    
}
