/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Produto;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cassiano
 */
public class ProdutosFacadeIT {
    
    public ProdutosFacadeIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of listProdutos method, of class ProdutosFacade.
     * @throws java.lang.Exception
     */
    @Test
    public void testListProdutos() throws Exception {
        System.out.println("listProdutos → must be 10");
        int expResult = 10;
        List result = ProdutosFacade.listProdutos();
        assertEquals(expResult, result.size());
    }

    /**
     * Test of getProduto method, of class ProdutosFacade.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetProduto() throws Exception {
        System.out.println("getProduto → id = 1, name = turbao");
        String id = "1";
        String expResult = "turbao";
        Produto result = ProdutosFacade.getProduto(id);
        assertEquals(expResult, result.getNome());
    }
    
}
