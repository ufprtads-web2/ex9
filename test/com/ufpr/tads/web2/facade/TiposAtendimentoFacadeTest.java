/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.TipoAtendimento;
import java.sql.SQLException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cassiano
 */
public class TiposAtendimentoFacadeTest {
    
    public TiposAtendimentoFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of listTiposAtendimentos method, of class TiposAtendimentoFacade.
     * @throws java.sql.SQLException
     */
    @Test
    public void testGetTipoAtendimento() throws SQLException {
        System.out.println("GetTipoAtendimento where id = 1 → completo");
        String res_esperado = "completo";
        TipoAtendimento tipo = new TipoAtendimento();
        tipo = TiposAtendimentoFacade.getTipoAtendimento("1");                
        assertEquals(res_esperado, tipo.getNome());               
    }    
    
    @Test
    public void testListTiposAtendimentos() throws SQLException {
        System.out.println("listTiposAtendimentos: Size must be 4");
        List<TipoAtendimento> lista = null;                
        List<TipoAtendimento> resultado = TiposAtendimentoFacade.listTiposAtendimentos();        
        int size = resultado.size();        
        assertEquals(4, size);               
    }    
}
