/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.resources;

import com.ufpr.tads.web2.facade.ProdutosFacade;
import com.ufpr.tads.web2.beans.Produto;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author cassiano
 */
@Path("produtos")
public class ProdutosResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProdutosResource
     */
    public ProdutosResource() {
    }

    /**
     * Retrieves representation of an instance of resources.ProdutosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produto> getJson() {
        
    List<Produto> resultado = null;
        try {
            resultado = ProdutosFacade.listProdutos();
        } catch (SQLException ex) {
            Logger.getLogger(ProdutosResource.class.getName()).log(Level.SEVERE, null, ex);
        }                    
        return resultado;
    }   
    
    @GET    
    @Produces(MediaType.APPLICATION_JSON)        
    @Path("/{id}")
    public Produto getProdutoJson(@PathParam("id") String id) {                
        Produto resultado = null;
        try {
            resultado = ProdutosFacade.getProduto(id);
        } catch (SQLException ex) {
            Logger.getLogger(ProdutosResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;        
    }

    /**
     * PUT method for updating or creating an instance of ProdutosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
