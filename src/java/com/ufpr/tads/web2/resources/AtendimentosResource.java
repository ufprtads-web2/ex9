/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.resources;

import com.ufpr.tads.web2.beans.Atendimento;
import com.ufpr.tads.web2.beans.Produto;
import com.ufpr.tads.web2.facade.AtendimentosFacade;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author cassiano
 */
@Path("atendimentos")
public class AtendimentosResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AtendimentosResource
     */
    public AtendimentosResource() {
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Atendimento> getJson() {
        List<Atendimento> resultado = null; 
        try {
            resultado = AtendimentosFacade.listAtendimentos();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(AtendimentosResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;              
    }

    @GET    
    @Produces(MediaType.APPLICATION_JSON)        
    @Path("/{id}")
    public Atendimento getAtendimentoJson(@PathParam("id") String id) {                
        Atendimento resultado = null;
        try {
            resultado = AtendimentosFacade.getAtendimento(id);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(AtendimentosResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    return resultado;
    
    }
    
    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateAtendimento(@PathParam("id") String id) throws ClassNotFoundException{        
        try {
            Atendimento att = AtendimentosFacade.getAtendimento(id);
            AtendimentosFacade.updateAtendimento(att);
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentosResource.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    
    
//    @PUT
//    @Consumes(MediaType.APPLICATION_JSON)
//    public void putJson(String content) {
//    }
}
