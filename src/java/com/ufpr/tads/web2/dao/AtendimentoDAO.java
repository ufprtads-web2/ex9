/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Atendimento;
import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.beans.Produto;
import com.ufpr.tads.web2.beans.TipoAtendimento;
import com.ufpr.tads.web2.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cassiano
 */
public class AtendimentoDAO {
    
        
    private Connection con;

    public AtendimentoDAO() {
        this.con = ConnectionFactory.getConnection();
    }    
    
    public List<Atendimento> selectAtendimentos() throws SQLException, ClassNotFoundException{
        List<Atendimento> resultados = new ArrayList<>();        
        String sql = "SELECT * FROM atendimento";
        PreparedStatement st = con.prepareStatement(sql);        
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            Atendimento Att = new Atendimento();
            Cliente cli = new Cliente();
            Produto prod = new Produto();
            Usuario user = new Usuario();
            TipoAtendimento tipo = new TipoAtendimento();
            
            cli.setId(rs.getInt("id_cliente"));            
            prod.setId(rs.getInt("id_produto"));
            user.setId_usuario(rs.getInt("id_usuario"));
            tipo.setId(rs.getInt("id_tipo_atendimento"));            
            
            
            Att.setId(rs.getInt("id"));   
            Att.setDsc(rs.getString("dsc"));
            Att.setResposta(rs.getString("res_atendimento"));            
            Att.setDt_hr(rs.getDate("dt_hr"));            
            Att.setProduto(prod);
            Att.setCliente(cli);
            Att.setUsuario(user);
            Att.setTipo_atendimento(tipo);
            resultados.add(Att);
        }        
        return resultados;      
    }
    
    public Atendimento selectAtendimento(String id) throws SQLException, ClassNotFoundException{
        
        String sql = "SELECT * FROM atendimento WHERE id=(?) limit 1";
        PreparedStatement st = con.prepareStatement(sql);  
        st.setString(1, id);
        ResultSet rs = st.executeQuery();
        Atendimento Att = new Atendimento();
        Cliente cli = new Cliente();
        Produto prod = new Produto();
        Usuario user = new Usuario();
        TipoAtendimento tipo = new TipoAtendimento();
        while (rs.next()){
            cli.setId(rs.getInt("id_cliente"));            
            prod.setId(rs.getInt("id_produto"));
            user.setId_usuario(rs.getInt("id_usuario"));
            tipo.setId(rs.getInt("id_tipo_atendimento"));            
            Att.setId(rs.getInt("id"));   
            Att.setDsc(rs.getString("dsc"));
            Att.setResposta(rs.getString("res_atendimento"));            
            Att.setDt_hr(rs.getDate("dt_hr"));            
            Att.setProduto(prod);
            Att.setCliente(cli);
            Att.setUsuario(user);
            Att.setTipo_atendimento(tipo);
            return Att;
        }
        return null;
    }
    
    public void removeAtendimento(String id) throws SQLException{
        String sql = "DELETE FROM atendimento WHERE id=(?) limit 1";
        PreparedStatement st = con.prepareStatement(sql);  
        st.setString(1, id);
        boolean rs = st.execute();          
    }
    

    public void updateAtendimento(Atendimento atendimento) throws SQLException{
        String sql = "UPDATE atendimento SET dsc=(?),res_atendimento=(?),id_cliente=(?),id_produto=(?),id_usuario=(?),id_tipo_atendimento=(?),dt_hr=(?) WHERE id=(?)";
        Usuario user = atendimento.getUsuario();
        Produto prod =  atendimento.getProduto();
        Cliente cli = atendimento.getCliente();
        TipoAtendimento tipo = atendimento.getTipo_atendimento();
        
        
        try (PreparedStatement st = con.prepareStatement(sql)) {
            
            st.setString(1, atendimento.getDsc());
            st.setString(2, atendimento.getResposta());
            st.setInt(3, cli.getId());   
            st.setInt(4, prod.getId());   
            st.setInt(5, user.getId_usuario());   
            st.setInt(6, tipo.getId());   
            st.setDate(7, atendimento.getDt_hr());            
            st.setInt(8, atendimento.getId());   
            st.executeUpdate();
        }             
    }
    
    public void insertAtendimento(Atendimento atendimento) throws SQLException{
        String sql = "INSERT into atendimento (id, dsc, res_atendimento, id_cliente, id_produto, id_usuario, id_tipo_atendimento) VALUES ((?),(?),(?),(?),(?),(?),(?))";
   
        try (PreparedStatement st = con.prepareStatement(sql)) {
                st.setInt(1, atendimento.getId());
                st.setString(2, atendimento.getDsc());
                st.setString(3, atendimento.getResposta());
                st.setInt(4, atendimento.getCliente().getId());
                st.setInt(5, atendimento.getProduto().getId());
                st.setInt(6, atendimento.getUsuario().getId_usuario());
                st.setInt(7, atendimento.getTipo_atendimento().getId());
                st.executeUpdate();
            
            
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }    
  
    
    public void closeConnection() throws SQLException{
        con.close();        
    }

    
}
