/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ufpr.tads.web2.facade.AtendimentosFacade;
import com.ufpr.tads.web2.facade.ProdutosFacade;
import com.ufpr.tads.web2.facade.ClientesFacade;
import com.ufpr.tads.web2.facade.TiposAtendimentoFacade;
import com.ufpr.tads.web2.beans.Atendimento;
import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.beans.LoginBean;
import com.ufpr.tads.web2.beans.Produto;
import com.ufpr.tads.web2.beans.TipoAtendimento;
import com.ufpr.tads.web2.beans.Usuario;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

/**
 *
 * @author cassiano
 */
@WebServlet(name = "AtendimentoServlet", urlPatterns = {"/AtendimentoServlet"})
public class AtendimentoServlet extends HttpServlet {
    
    private final String errorMsg = "Usuário deve se autenticar para acessar o sistema";
    private String id = null;
    private String action = null;
    private String forward_page = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ClassNotFoundException {     
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");        
        try {
            HttpSession session = request.getSession();
            LoginBean usuario = (LoginBean)session.getAttribute("login");        
            
            if (usuario == null){                
                request.setAttribute("errorMsg",errorMsg);        
                rd.forward(request, response);
            }
            
            action = (String) request.getParameter("action");            
            id = request.getParameter("id");
            Atendimento atendimento = new Atendimento();
            
            if (action == null){
                action = "LIST";
            }
            
            switch(action) {
                case "LIST":
                    forward_page = "/atendimentosListar.jsp";
                    List<Atendimento> resultado = AtendimentosFacade.listAtendimentos();                    
                    request.setAttribute("atendimentos", resultado);                    
                    break;
                case "SHOW":
                    forward_page = "/atendimentoDetalhes.jsp";
                    atendimento = AtendimentosFacade.getAtendimento(id);
                    if (atendimento != null){                        
                        request.setAttribute("atendimento", atendimento);                        
                    }
                    break;

                case "FORMNEW":
                    forward_page = "/atendimento.jsp";
                    List<Produto> produtos = ProdutosFacade.listProdutos();
                    List<Cliente> clientes = ClientesFacade.listClients();
                    List<TipoAtendimento> tipos = TiposAtendimentoFacade.listTiposAtendimentos();                    
                    request.setAttribute("produtos", produtos);
                    request.setAttribute("clientes", clientes);
                    request.setAttribute("tipos", tipos);
                    break;
                case "NEW":                    
                    forward_page = "/AtendimentoServlet?action=LIST";
                    try {                                            
                    Cliente cli = new Cliente();
                    Usuario user = new Usuario();
                    Produto prod = new Produto();
                    TipoAtendimento atr = new TipoAtendimento();                    
                    int uid = Integer.parseInt(request.getParameter("user_id"));
                    int cid = Integer.parseInt(request.getParameter("Cliente"));
                    int pid = Integer.parseInt(request.getParameter("Produto"));
                    int atrid = Integer.parseInt(request.getParameter("Tipo"));
                    user.setId_usuario(uid);
                    cli.setId(cid);                    
                    prod.setId(pid);
                    atr.setId(atrid);
                    
                  
                    atendimento.setUsuario(user);
                    atendimento.setCliente(cli);
                    atendimento.setProduto(prod);
                    atendimento.setTipo_atendimento(atr);
                    atendimento.setDsc(request.getParameter("dsc"));
                    String res = request.getParameter("resposta");
                    if (res != null && res.equals("true")){
                        atendimento.setResposta("s");                    
                    }else
                        atendimento.setResposta("n");                    
                    AtendimentosFacade.addAtendimento(atendimento);
                    break;
                    }catch (Exception e) {
                        e.printStackTrace();
                        throw e;
                    }
                }            
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }finally{
            rd = getServletContext().getRequestDispatcher(forward_page);
            rd.forward(request, response);
        }
    }
  
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(AtendimentoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(AtendimentoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
