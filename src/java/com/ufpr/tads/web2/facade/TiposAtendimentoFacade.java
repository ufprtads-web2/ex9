/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.TipoAtendimento;
import com.ufpr.tads.web2.dao.TipoAtendimentoDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cassiano
 */
public class TiposAtendimentoFacade {
    
    static TipoAtendimentoDAO dao = null;
    static TipoAtendimento tipo = null;
    
    public static List listTiposAtendimentos() throws SQLException{        
        dao = new TipoAtendimentoDAO();
        List<TipoAtendimento> tipos;
        try {            
            tipos = dao.selectTipoAtendimentos();
            return tipos;
        } catch (SQLException e) {
           Logger.getLogger(TiposAtendimentoFacade.class.getName()).log(Level.SEVERE, null, e); 
        }finally {
            dao.closeConnection();    
        }       
        return null;
    }
    
    public static TipoAtendimento getTipoAtendimento(String id) throws SQLException{        
        tipo = null;
        if (id != null){
            dao = new TipoAtendimentoDAO();        
            try {                                    
                tipo = dao.selectTipoAtendimento(id);
            } catch (SQLException ex) {
                Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally {
            dao.closeConnection();
            }            
        }
        return tipo;        
    }
}